﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Shimeji
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Topmost = true;
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom - this.Height;
            MouseLeftButtonDown += new MouseButtonEventHandler(MouseButtonDownHandler);
        }
        private void MouseButtonDownHandler(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        void CloseWindow(object sender, RoutedEventArgs e)
        {
            base.Close();
        }

        private void menuSelect(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)e.Source;
            String name = (String)item.Header;
            switch (name)
            {
                case "번역기":
                    {
                        break;
                    }
                case "Android Studio":
                case "Visual Studio":
                    execute(getDevelopExecutePath(name));
                    break;
                case "WOT":
                case "osu!":
                    execute(getGameExecutePath(name));
                    break;
                default: break;
            }
        }

        private void execute(String path)
        {
            System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();
            System.Diagnostics.Process pro = new System.Diagnostics.Process();
            info.FileName = path;
            info.CreateNoWindow = true;
            info.UseShellExecute = false;
            info.RedirectStandardError
                = info.RedirectStandardInput
                = info.RedirectStandardOutput
                = true;
            pro.StartInfo = info;
            pro.Start();
        }

        private String getGameExecutePath(String name)
        {
            switch (name)
            {
                case "WOT":
                    return "D:\\Play\\Games\\WorldOfTank_KR\\WorldOfTanks.exe";
                case "osu!":
                    return "D:\\Play\\osu!\\osu!.exe";
                default: break;
            }
            return "";
        }

        private String getDevelopExecutePath(String name)
        {
            switch (name)
            {
                case "Android Studio":
                    return "M:\\Tool\\Develop\\Android\\Studio\\bin\\studio64.exe";
                case "Visual Studio":
                    return "M:\\Tool\\Develop\\Visual Studio 2015\\Common7\\IDE\\devenv.exe";
                default: break;
            }
            return "";
        }
    }
}
